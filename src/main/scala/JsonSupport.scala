import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import models.User
import spray.json.DefaultJsonProtocol

trait JsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val userJsonFormat = jsonFormat4(User)
}
