import UserRegistryActor._
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import models.User

import scala.concurrent.duration.DurationInt
import akka.event.Logging
import dao.UserDao

trait UserApi extends JsonSupport {
  implicit def system: ActorSystem
  lazy val log = Logging(system, classOf[UserApi])
  def userRegistryActor: ActorRef

  implicit lazy val timeout = Timeout(5.seconds)

  val userRoutes: Route = {

    pathPrefix("users")  {
      pathEndOrSingleSlash {
        get {
          //complete(StatusCodes.OK)
          //val x = (UserDao.selectAllUsers()).mapTo[Seq[User]]
          complete((userRegistryActor ? GetAllUsers).mapTo[Seq[User]])
          //complete(x)
        }~
          post {
            entity(as[User]) { user =>
              val userCreated = (userRegistryActor ? CreateUser(user)).mapTo[ActionPerformed]
              onSuccess(userCreated) { performed =>
                log.info(s"Created User [${user.fname}]: ${performed.action}")
                complete(StatusCodes.Created, performed.action)
              }
            }
          }
      }~
        path(LongNumber){id=>
          get{
            val result= (userRegistryActor ? GetUserById(id)).mapTo[Seq[User]]
            rejectEmptyResponse{
              complete(result)
            }
          }~
            delete {
              val result = (userRegistryActor ? DeleteUser(id)).mapTo[ActionPerformed]
              onSuccess(result) { performed =>
                log.info(s"Deleted user $id", performed.action)
                complete(StatusCodes.OK)
              }
            }
        }
    }
  }
}
/*
pathPrefix("users")  {
      get {
        pathEndOrSingleSlash {
          complete((userRegistryActor ? getAllUsers()).mapTo[Seq[User]])
        } ~
          path(LongNumber) { id =>

            val x = (userRegistryActor ? getUserById(id))
            println("fetching with id =" + id + "\tresult= " + x)
            complete(
              (userRegistryActor ? getUserById(id)).mapTo[Seq[User]]
            )
          }
      }~
        post{
          pathEndOrSingleSlash{
            entity(as[User]) { user =>
              onSuccess((userRegistryActor ? createUser(user)).mapTo[Seq[User]]) {perf=>
                complete(StatusCodes.Created,perf)
              }
              }
          }
        }~
        delete{
          path(LongNumber){id=>

            val x= (userRegistryActor ? deleteUser(id)).mapTo[Seq[User]]

            onComplete((userRegistryActor ? deleteUser(id)).mapTo[Seq[User]]) {
              case Success(deletedUser) => {
                println("id to be deleted ="+ id+"\twith result = "+deletedUser)
                complete(StatusCodes.Created, deletedUser)
              }
              case Failure(ex) => complete(StatusCodes.InternalServerError)
            }
          }
        }~
        put{
          path(LongNumber){id=>
            entity(as[User]){newUser=>
              reject
//                  onComplete((userRegistryActor ? updateUser(id,newUser)).mapTo[Seq[User]]){
//                    case Success(updatedUser)=> {
//                      println("updated user with id="+id+"\t"+updatedUser)
//                      complete(StatusCodes.Created, updatedUser)
//                    }
//                    case Failure(ex) => complete(StatusCodes.InternalServerError)
//                  }

            }
          }
        }
}
}

*/


//case Success(registeredUser) =>
//                    {
//                      println("entity for POST ="+user+"\twith result "+registeredUser)
//                      complete(StatusCodes.Created, registeredUser)
//                    }
//                    case Failure(ex) => complete(StatusCodes.InternalServerError)
//                  }
//complete((userRegistryActor?createUser(user)).map(usr=>StatusCodes.Created,))
