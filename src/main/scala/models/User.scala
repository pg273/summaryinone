package models

case class User(fname:String,lname:String,email:String,id:Long = 0L)

case class Users(users: Seq[User])