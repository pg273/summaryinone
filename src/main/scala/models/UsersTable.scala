package models

import slick.jdbc.PostgresProfile.api._

class UserTable(tag:Tag) extends Table[User](tag,"myuser"){
  def fname=column[String]("fname")
  def lname=column[String]("lname")
  def email=column[String]("email")
  def id=column[Long]("id",O.PrimaryKey,O.AutoInc)
  override def * = (fname,lname,email,id)<> (User.tupled,User.unapply)
}