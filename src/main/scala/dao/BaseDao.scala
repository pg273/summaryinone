package dao

import config.Config
import models._
import slick.dbio.NoStream
import slick.lifted.TableQuery
import slick.sql.{ FixedSqlStreamingAction, SqlAction}

import scala.concurrent.Future

trait BaseDao extends Config   {
  var usersTable=TableQuery[UserTable]

  //implicit functions to run queries using db.run

  protected def executeRun[A](action: SqlAction[A,NoStream,_ <: slick.dbio.Effect]):Future[A]=db.run(action)
   def executeReadRun[A](action: FixedSqlStreamingAction[Seq[A],NoStream,_ <: slick.dbio.Effect]):Future[Seq[A]]=db.run(action)
}
