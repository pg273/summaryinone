package dao

import models.User
import slick.jdbc.PostgresProfile.api._

object UserDao extends BaseDao{
  def selectAllUsers()=db.run(usersTable.result)

  def selectById(id:Long)=db.run(usersTable.filter(_.id === id).result)

  def createUser(user:User)={
    println("3")
    db.run(usersTable returning usersTable.map(_.id) += user)}
  // db.run(usersTable+=user)

  def removeUserById(id:Long)= db.run(usersTable.filter(_.id===id).delete)

  def updateUserById(id:Long,newUser:User)={
    db.run(usersTable.filter(_.id === newUser.id).update(newUser))
  }
//db.run(usersTable += user)
  //    val updateQuery=usersTable.filter(_.id===id).map( user => (user.fname,user.lname,user.email)).update((newUser.fname,newUser.lname,newUser.email))
  //    db.run(updateQuery)
}
