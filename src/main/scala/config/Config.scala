package config

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.ConfigFactory
import dao.UserDao.usersTable
import models.User
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


trait Config  {
  val db = Database.forConfig("database")

//  implicit val system: ActorSystem = ActorSystem("helloAkkaHttpServer")
//  implicit val materializer: Materializer = ActorMaterializer()
//  val databaseUrl= ((ConfigFactory.load()).getConfig("database")).getString("url")
//  val databaseUser= ((ConfigFactory.load()).getConfig("database")).getString("user")
//  val databasePassword= ((ConfigFactory.load()).getConfig("database")).getString("password")

//  implicit val session: Session = db.createSession()
}
