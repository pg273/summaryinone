import akka.actor.{Actor, ActorLogging}
import dao.UserDao
import models.{User, Users}

import scala.util.{Failure, Success}

object UserRegistryActor{
  final case class GetAllUsers()
  final case class GetUserById(id:Long)
  final case class CreateUser(user:User)
  final case class DeleteUser(id:Long)
  final case class UpdateUser(id:Long, user:User)
  final case class ActionPerformed(action: String)
}
class UserRegistryActor extends Actor with ActorLogging with JsonSupport {
  import UserRegistryActor._
  import context.dispatcher

  override def receive: Receive = {

    case GetAllUsers =>{
      log.info("searching for all users")
      val mysender = sender
      val allUsers = UserDao.selectAllUsers()
      allUsers.onComplete {
        case Success(usr) => mysender ! usr
        case Failure(failureUsr) => println("Data not found to find all Users in Database => "+failureUsr)
      }
    }


    case GetUserById(id:Long)=>
      val mysender = sender
      val user = UserDao.selectById(id)
      user.onComplete {
        case Success(usr) => mysender ! usr
        case Failure(failureUsr) => println("Data not found to find User (by id) in Database with exception\n"+failureUsr)
      }

    case CreateUser(user:User)=>
      log.info(s"Creating user ${user.fname} ${user.id}")
      UserDao.createUser(user)
      sender() ! ActionPerformed(s"User ${user.fname} created.")

    case DeleteUser(id:Long)=>
      log.info(s"Creating user with id = ${id}")
      val mysender = sender
      val futureResult= UserDao.removeUserById(id)
      futureResult.onComplete {
        case Success(usr) => mysender ! ActionPerformed(s"User $id deleted")
        case Failure(failureUsr) => log.info(s"User with id =  $id could not be deleted")
          failureUsr.printStackTrace()
      }

    case UpdateUser(id:Long,user:User)=>
      val mysender = sender
      val futureResult= UserDao.updateUserById(id,user).mapTo[Seq[User]]
      futureResult.onComplete {
        case Success(usr) => mysender ! usr
        case Failure(failureUsr) => log.info(s"User with id =  $id could not be updated")
          failureUsr.printStackTrace()
      }
  }


}
