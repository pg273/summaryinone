import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import scala.concurrent.duration.Duration
import slick.jdbc.meta.MTable
import scala.concurrent.{Await, Future}
import config.{Config, MigrationConfig}
import dao.UserDao.usersTable
import models.User
import scala.util.{Failure, Success}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext

object Main extends App with Config with MigrationConfig with UserApi {

//    def createSchemaIfNotExists()={
//      db.run(MTable.getTables).flatMap {
//        case tables if tables.isEmpty=> db.run(usersTable.schema.create).andThen{
//        case Success(_)=>println("Table created")
//          }
//        case tables if tables.nonEmpty=> println("Table already exists")
//                                          Future.successful()
//      }
//    }
//
//    def addInitialData():Future[Unit]  ={
//      val setAction=DBIO.seq(
//        usersTable += User("Punam","Gunjal","emai l1@gmail.com"),
//        usersTable += User("xxx","yyy","email2@gmail.com")
//      )
//      db.run(setAction).andThen {
//        case Success(_) => println("Initial data added")
//        case Failure(e) => e.printStackTrace()
//      }
//    }
//      val future=createSchemaIfNotExists().flatMap(_=>addInitialData())
//      Await.result(future,Duration.Inf)

  implicit val system: ActorSystem = ActorSystem("helloAkkaHttpServer")
  implicit val executionContext: ExecutionContext = system.dispatcher
  try{
    val setAction=DBIO.seq(
      usersTable.schema.createIfNotExists,
      usersTable += User("Punam","Gunjal","email1@gmail.com")
    )
    //      usersTable += User("Poonam","G","email2@gmail.com"),
    //      usersTable += User("Enovate","IT","email2@gmail.com")

    val queryFuture = db.run(setAction)
    queryFuture.onComplete {
      case Success(_) => println("Database seeded")
      case Failure(e) => e.printStackTrace()
    }
  } finally db.close

  val userRegistryActor: ActorRef = system.actorOf(Props[UserRegistryActor],"UserRegistryActor")

  lazy val routes: Route = userRoutes

  //val bindingFuture = Http().bindAndHandle(routes,"localhost",9001)
  val bindingFuture = Http().newServerAt("localhost", 8080).bind(routes)

  println(s"Server online\n Press RETURN to stop...")
  scala.io.StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete{_ => db.close(); system.terminate()}
}